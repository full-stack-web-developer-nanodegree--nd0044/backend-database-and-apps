# "Database code" for the DB Forum.

import psycopg2
import bleach

DB_NAME = "forum"


def get_posts():
    """Return all posts from the 'database', most recent first."""
    conn = psycopg2.connect(database=DB_NAME)
    cursor = conn.cursor()
    cursor.execute("SELECT content, time FROM posts ORDER BY time DESC")
    return cursor.fetchall()
    cursor.close()


def add_post(content):
    """Add a post to the 'database' with the current timestamp."""
    conn = psycopg2.connect(database=DB_NAME)
    cursor = conn.cursor()
    clean_content = bleach.clean(content)
    cursor.execute("INSERT INTO posts VALUES (%s)", (clean_content,))
    conn.commit()
    conn.close()
